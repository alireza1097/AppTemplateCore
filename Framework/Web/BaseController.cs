﻿using System;
using System.Collections.Generic;
using System.Text;
using AppTemplateCore.Framework.Commands;
using AppTemplateCore.Framework.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Framework.Web
{
    public class BaseController : Controller
    {
        protected readonly CommandDispatcher _commandDispatcher;
        protected readonly QueryDispatcher _queryDispatcher;

        public BaseController(CommandDispatcher commandDispatcher, QueryDispatcher queryDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }
    }
}

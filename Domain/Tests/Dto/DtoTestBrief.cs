﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tests.Dto
{
    public class DtoTestBrief
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string InsertDateTime { get; set; }
    }
}

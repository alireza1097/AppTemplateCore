﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tests
{
    public class Test : BaseEntity
    {
        public string Title { get; set; }
        public string InsertDateTime { get; set; }
        public bool ActiveFlag { get; set; }
    }
}

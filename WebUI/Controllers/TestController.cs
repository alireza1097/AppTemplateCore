﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTemplateCore.Framework.Commands;
using AppTemplateCore.Framework.Queries;
using Domain.Tests.Dto;
using Domain.Tests.Query;
using Framework.Web;
using Microsoft.AspNetCore.Mvc;
using WebUI.ViewModels.Test;

namespace WebUI.Controllers
{
    public class TestController : BaseController
    {
        public TestController(CommandDispatcher commandDispatcher, QueryDispatcher queryDispatcher) : base(commandDispatcher, queryDispatcher)
        {
        }

        public IActionResult Index()
        {
            var lstTest = _queryDispatcher.Dispatch<List<DtoTestBrief>>(new TestGetAllQuery());
            var model = lstTest.AsEnumerable().Select(x => new TestViewModel()
            {
                Id = x.Id,
                Title = x.Title,
                InsertDateTime = x.InsertDateTime
            }).ToList();

            return View(model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.Test
{
    public class TestViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string InsertDateTime { get; set; }
    }
}

﻿using AppTemplateCore.Framework.Queries;
using Constract.Tests;
using Domain.Tests.Dto;
using Domain.Tests.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationServices.Tests.Queries
{
    public class TestGetAllQueryHandler : IQueryHandler<TestGetAllQuery, List<DtoTestBrief>>
    {
        private readonly ITestQueryRepository testQueryRepository;

        public TestGetAllQueryHandler(ITestQueryRepository testQueryRepository)
        {
            this.testQueryRepository = testQueryRepository;
        }

        public List<DtoTestBrief> Handle(TestGetAllQuery query)
        {
            return testQueryRepository.TestGetAll();
        }
    }
}

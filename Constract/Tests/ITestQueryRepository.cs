﻿using Domain.Tests.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Constract.Tests
{
    public interface ITestQueryRepository
    {
        List<DtoTestBrief> TestGetAll();
    }
}

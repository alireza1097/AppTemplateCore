﻿using Constract.Tests;
using Domain.Tests;
using Domain.Tests.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DataAccessLayer.Tests
{
    public class FakeTestQueryRepository : ITestQueryRepository
    {
        static List<Test> test = new List<Test>()
        {
            new Test()
            {
                Id = 1,
                Title = "Test One",
                InsertDateTime = "1400/02/29",
                ActiveFlag = true
            }
        };

        public List<DtoTestBrief> TestGetAll()
        {
            return test.AsEnumerable().Select(x => new DtoTestBrief()
            {
                Id = x.Id,
                Title = x.Title,
                InsertDateTime = x.InsertDateTime
            })
                .ToList();
        }
    }
}
